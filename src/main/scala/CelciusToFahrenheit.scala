import scala.io.StdIn.readLine

object CelciusToFahrenheit {
  def main(strings: Array[String]): Unit =
  {
    println("---CelciusToFahrenheitConverter---")
    println("Please enter a number to convert:")
    val userInput = readLine().toDouble
    val results = converter(userInput)
    println(s"The result is $results F")
    
  }

  def converter(celcius: Double) = {
    (celcius * 9 / 5) + 32
  }
  
}
