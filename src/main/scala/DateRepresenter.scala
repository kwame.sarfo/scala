object DateRepresenter {

  def main(strings: Array[String]) =
    {
      println(dateRepresenter("01/02/15"))
    }

  def dateRepresenter(date:  String) =
    {
      val splittedDates = date.split('/')
      val absOfDay = splittedDates(0).toInt.abs
      val month = splittedDates(1).toInt
      val year = splittedDates(2).toInt

      val dayInWords = absOfDay match {
        case 1|21|31 => "st"
        case 2|22 => "nd"
        case 3|23 => "rd"
        case _ => "th"
      }

      val monthInwords = month match {
        case 1 => "January"
        case 2 => "February"
        case 3 => "March"
        case 4 => "April"
        case 5 => "May"
        case 6 => "June"
        case 7 => "July"
        case 8 => "August"
        case 9 => "September"
        case 10 => "October"
        case 11 => "November"
        case 12 => "December"
        case _ => "invalid entry"
      }

        s"$absOfDay$dayInWords $monthInwords 20$year"

    }

}
