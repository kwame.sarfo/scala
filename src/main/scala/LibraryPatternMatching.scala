object LibraryPatternMatching {

  def main(string: Array[String]) =
    {
      canBorrow("Junior", 4)
    }


  def canBorrow(membership:String, booksToBorrow:Int): Unit =
  {
    membership match {
      case "Junior" if booksToBorrow > 5 => println("Cannot borrow.")
      case "Medium" if booksToBorrow > 10 => println("Cannot borrow.")
      case "Senior" if booksToBorrow > 15 => println("Cannot borrow.")
      case _ => println("Can borrow.")
    }
  }
}
