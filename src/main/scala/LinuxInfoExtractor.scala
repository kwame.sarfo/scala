object LinuxInfoExtractor {
  def main(strings: Array[String]) =
  {
    linuxInfoExtractor("root:x:0:0:root:/root:/bin/bash")
  }

  def linuxInfoExtractor(line:String) =
  {
    line match {
      case s"$username:$password:$userid:$groupid:$description:$homedirectory:$shellTouse" => {
            println (s"username: $username")
            println (s"password: $password")
            println (s"userid: $userid")
            println (s"groupid: $groupid")
            println (s"description: $description")
            println (s"homeDirectory: $homedirectory")
            println (s"shellToUse: $shellTouse")
    }
    }
  }
}
