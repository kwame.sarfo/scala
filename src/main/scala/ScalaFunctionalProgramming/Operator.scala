package ScalaFunctionalProgramming

import scala.io.StdIn.readLine

object Operator {
  def main(strings: Array[String]):Unit = {
    val add = findOperation("add")
    val subtract = findOperation("subtract")
    val power = findOperation("power")

    println(add(1,2))

  }

  def findOperation(command:String) :(Int, Int)=>Int = {
    command match {
      case "add" => (x:Int, y:Int) => x + y
      case "subtract" => (x:Int, y:Int) => x - y
      case "power" => (x:Int, y:Int) => Math.pow(x, y).toInt
    }
  }
}
