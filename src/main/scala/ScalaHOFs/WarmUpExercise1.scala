package ScalaHOFs

import scala.Function5

//Write a function value that takes five strings and returns the sum of the length of all
//of the strings. Write this function using both long hand syntax (e.g. FunctionX) and
//  short hand. Which do you prefer and why?


object WarmUpExercise1 {
  val lengthCalculator = new Function5[String,String,String,String,String,Int] {
    def apply(x:String, y:String, z:String, a:String, b:String): Int = x.length + y.length + z.length + a.length + b.length
  }

  val lengthCalculator1 = (x:String, y:String, z:String, a:String, b:String) => x.length + y.length + z.length + a.length + b.length


  def main(strings:Array[String]):Unit = {
    println(lengthCalculator("hello","hello","hello","hello","hello"))
    println(lengthCalculator1("hello","hello","hello","hello","hello"))
  }
}
