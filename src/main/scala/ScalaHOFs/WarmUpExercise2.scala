package ScalaHOFs

//Write a function that takes an int as an argument and returns another function as a
//  result

object WarmUpExercise2 {
  def funcTakeIntArgReturnsFunc(arg:Int): () => Int = {
    () => arg
  }

  def main(string:Array[String]):Unit = {
    val f = funcTakeIntArgReturnsFunc(5)
    print(f())
  }
}
