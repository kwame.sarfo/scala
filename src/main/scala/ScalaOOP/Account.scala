package ScalaOOP

import ScalaOOP.MembershipType.MembershipType

import java.time.LocalDate

class Account(var totalValueOfAllTrades:Float = 0,
              var trades:Array[Trade] = new Array[Trade](10),
              var membershipType: MembershipType = MembershipType.NONE,
              var maxDailyTradeValueLimit:Double = 10000,
              var totalTradeCountToday:Int = 0,
              var currentDate:LocalDate = LocalDate.now(),
              var points:Int = 0
             )
