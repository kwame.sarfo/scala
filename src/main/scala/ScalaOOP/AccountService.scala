package ScalaOOP

object AccountService {
  def addValueOfTrade(account:Account, trade: EquityTrade) =
    {
      account.totalValueOfAllTrades += trade.quantity * trade.price
    }
    
  def addTradeToList(account: Account, trade: Trade) =
    {
      account.trades :+ trade
    }
    
  def increaseTradeCount(account: Account) =
    {
      account.totalTradeCountToday += 1
    }
    
  def increaseAccountPoints(account: Account) =
    {
      account.points += 1
    }

  def upgradeMembership(account: Account): Unit = {
    if (account.points < 10 && account.points > 0) {
      account.membershipType = MembershipType.BRONZE
    }
    else if (account.points < 19 && account.points >= 10) {
      account.membershipType = MembershipType.SILVER
    }
    else if (account.points >= 20) {
      account.membershipType = MembershipType.GOLD
    }
  }

}
