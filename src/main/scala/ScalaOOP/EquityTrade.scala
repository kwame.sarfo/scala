package ScalaOOP

class EquityTrade(id:String,
                  val symbol:String,
                  val quantity:Int,
                  price:Float,
                  d:Float) extends Trade(id, price)
{
  override val dividend: Float = d

  override def isExecutable(): Boolean = true

  def value = price*quantity
  override def toString = s"EquityTrade($symbol, $quantity)"
}

