package ScalaOOP

class FXTrade(id:String,
              price:Float,
              d:Float) extends Trade(id, price)
{
  override val dividend: Float = d

  override def isExecutable(): Boolean = false


  override def toString = s"FXTrade()"
}
                
