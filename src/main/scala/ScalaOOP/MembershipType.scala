package ScalaOOP

object MembershipType extends Enumeration
{
  type MembershipType = Value

  val NONE = Value(1, "null")
  val BRONZE = Value(5, "8")
  val SILVER = Value(10, "null")
  val GOLD = Value(20, "null")

}

enum MemberType(limit:Int, timeRestriction:Int) {
  case Junior extends MemberType(5, 5)
}
