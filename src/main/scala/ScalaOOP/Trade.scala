package ScalaOOP

import java.time.{LocalDate, LocalTime}

abstract class Trade(val id:String,
                     private var p:Float,
            val tradeDate:LocalDate = LocalDate.now(),
            val tradeHour:Int = LocalTime.now().getHour)
{
  def price = p

  def price_=(value:Float) =
    {
      if(value > 0.0)
        {
          p = value
        }
    }
  def dividend:Float

  def isExecutable():Boolean

}
