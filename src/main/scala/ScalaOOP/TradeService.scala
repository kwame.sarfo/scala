package ScalaOOP

object TradeService {
  def calcDividend(trade: EquityTrade) =
    {
      trade.dividend * trade.price * trade.quantity
    }

}
