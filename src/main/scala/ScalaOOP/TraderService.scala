package ScalaOOP
import java.time.{LocalDate, LocalTime}

object TraderService {
  def addTrade(trader: Trader, trade: Trade):String =
    {
      if(canTrade(trader))
        {
          if (trade.isInstanceOf[EquityTrade]) { AccountService.addValueOfTrade(trader.account, trade.asInstanceOf[EquityTrade])}
          AccountService.addTradeToList(trader.account, trade)
          AccountService.increaseTradeCount(trader.account)
          AccountService.increaseAccountPoints(trader.account)
          AccountService.upgradeMembership(trader.account)
          println("Trade added successfully.")
          return "Trade added successfully."
        }
      else
        {
          println("You are not eligible to trade.")
          return "You are not eligible to trade."
        }
    }


  def canTrade(trader: Trader): Boolean = {
    if (trader.account.membershipType != MembershipType.NONE
      && trader.account.membershipType.toString != "null"
      && LocalTime.now().getHour < trader.account.membershipType.toString.toInt)  false

    else if (trader.account.totalTradeCountToday < trader.account.membershipType.id &&
      trader.account.totalValueOfAllTrades < trader.account.maxDailyTradeValueLimit)  true

    else if (LocalDate.now().isAfter(trader.account.currentDate)) {
      trader.account.totalTradeCountToday = 0
      trader.account.totalValueOfAllTrades = 0
      trader.account.currentDate = LocalDate.now()
      canTrade(trader)
    }

    else false
  }
}
