package ScalaOOP

class Transaction(id:String,
                  symbol:String,
                  quantity:Int,
                  price:Float,
                  dividend:Float) extends EquityTrade(id, symbol, quantity, price, dividend)
                  with FeePayable with Taxable
{
  def amount() = {
    val feePayable = super.fee
    val taxPercent = super.tax
    val value = super.value
    
    val tax = taxPercent * (feePayable + value)
    
    tax + feePayable + value
  }
}
