package ScalaRecursion

object Factorial {

  //1 * 2 * 3 * 4 * 5
  def factorialBad(n:BigInt):BigInt = {
    if(n <= 1) 1
    else n* factorialBad(n-1)
  }

  def factorial(n: BigInt): BigInt = {
    def factorialAcc(n: BigInt, acc: BigInt): BigInt = {
      if(n <=1) acc
      else factorialAcc(n-1, n*acc)
    }
    factorialAcc(n, 1)
  }

  def main(strings:Array[String]) = {
    println(factorialBad(5))
    println(factorial(5))
  }
}
