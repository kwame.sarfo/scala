package ScalaRecursion

object Fibonacci {
  // 1,1,2,3,5,8,13,21,34,55,89
  //works it from right to left
  def fibonacci(n:BigInt): BigInt = {
    if(n<=1) 1
    else fibonacci(n-1) + fibonacci(n-2)
  }

  //works it from left to right
  def fibonacciTailRec(n:BigInt): BigInt = {
    def fibonacciAcc(n: BigInt, previous:BigInt, current:BigInt): BigInt = {
      if(n<=1) current
      else fibonacciAcc(n-1, current, current + previous)
    }

    fibonacciAcc(n, 1, 1)

  }

  def main(strings:Array[String]) = {
    println(fibonacci(5))
    println(fibonacciTailRec(5))
  }

}
