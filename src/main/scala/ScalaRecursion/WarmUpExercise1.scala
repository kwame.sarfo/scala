package ScalaRecursion

// Returns the sum of the numbers between two numbers a and b
// eg 3 and 7
// expect 4+5+6 = 15

object WarmUpExercise1 {

  def sumBtnA_BBad(a: Int, b:Int): Int =
  {
    if(b-1<=a) 0
    else b-1 + sumBtnA_BBad(a, b-1)
  }

  def sumBtnA_BGood(a:Int, b:Int) = {
    def sumBtnA_BGoodAcc(a:Int, acc:Int): Int = {
      if(a+1>=b) acc
      else sumBtnA_BGoodAcc(a+1, acc+a+1)
    }
    sumBtnA_BGoodAcc(a, 0)
  }

  def main(string:Array[String]) = {
    println(sumBtnA_BBad(3,7))
    println(sumBtnA_BGood(3,7))
    println(sumBtnA_BBad(3,7) == sumBtnA_BGood(3,7))
  }
}
