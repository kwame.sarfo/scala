package ScalaRecursion

//Concatenate a given string n times
// given "Hello" to be concatenated 3 times
// expect "HelloHelloHello"

object WarmUpExercise2 {
  def concatenationBad(str:String, times:Int):String = {
    if(times<=0) ""
    else str + concatenationBad(str, times-1)
  }

  def concatenationGood(str:String, times:Int) = {

    def concatenationGoodAcc(str: String, t:Int, acc:String):String = {
      if (t<=0) acc
      else concatenationGoodAcc(str, t-1, acc+str)
    }
    concatenationGoodAcc(str, times, "")
  }

  def main(string:Array[String]) = {
    println(concatenationBad("Hello", 3))
    println(concatenationGood("Hello", 3))
    println(concatenationBad("Hello", 3) == concatenationGood("Hello", 3))
  }
}
