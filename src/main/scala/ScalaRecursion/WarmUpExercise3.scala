package ScalaRecursion

//Find the length of a string
//given "hello"
// expect 5

object WarmUpExercise3 {
  def lfBad(st : String, index:Int):Int = {
    try {
      st(index)
    }
    catch {
      case e: Exception => return index
    }

    lfBad(st, index+1)
  }

  def lengthFinder(str:String) = {
    def lenghtAcc(s:String, acc:Int):Int = {

      try {
        s(acc)
      }
      catch {
        case e: Exception => return acc
      }


      lenghtAcc(s, acc+1)
    }

    lenghtAcc(str, 0)
  }

  def main(strings:Array[String]):Unit = {
    println(lengthFinder("Hello"))
    println(lfBad("Hello", 0))
    println(lengthFinder("Hello") == "Hello".length)
  }
}
