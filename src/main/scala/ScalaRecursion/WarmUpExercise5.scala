package ScalaRecursion



//Returns whether a given number n is prime (i.e. it is divisible by itself and 1)

object WarmUpExercise5 {

  def checkPrime(num:Int) = {
    def checkPrimeAcc(num:Int, acc:Int):Boolean = {
      if (acc != 1
        &&acc != num && num%acc == 0) false
      else if(acc>=num) true
      else checkPrimeAcc(num, acc+1)
    }
    checkPrimeAcc(num, 1)
  }

  def main(string: Array[String]): Unit = {
    println(checkPrime(9))
  }
}
