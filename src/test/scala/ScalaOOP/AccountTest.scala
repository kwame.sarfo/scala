package ScalaOOP

import org.scalatest.funsuite.AnyFunSuite

import java.time.LocalDate

class AccountTest extends AnyFunSuite {

  test("newTraderHasZeroPoints") {
    val traderAccount = new Account()
    val trader = new Trader("John", "Doe", traderAccount)
    assert(trader.account.points == 0)
  }

  test("newTraderHasNoMembership") {
    val traderAccount = new Account()
    val trader = new Trader("John", "Doe", traderAccount)
    assert(trader.account.membershipType == MembershipType.NONE)
  }
}
