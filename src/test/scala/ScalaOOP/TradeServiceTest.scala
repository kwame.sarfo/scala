package ScalaOOP

import org.scalatest.funsuite.AnyFunSuite

class TradeServiceTest extends AnyFunSuite {
  test("TradeService.calcDividend") {
    val newTrade = new EquityTrade("uid32", "AAPL", 3, 3.2f, 0.125f)
    val dividend = TradeService.calcDividend(newTrade)
    assert(1.2f == dividend)
  }

}
