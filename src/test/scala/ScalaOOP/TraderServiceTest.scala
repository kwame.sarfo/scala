package ScalaOOP

import java.time.LocalDate
import org.scalatest.funsuite.AnyFunSuite



class TraderServiceTest extends AnyFunSuite {

  test("clientPointIncreasesAfterTrade") {
    val traderAccount = new Account()
    val trader = new Trader("John", "Doe", traderAccount)

    val newTrade = new EquityTrade("uid32", "AAPL", 3, 3.2f, 0.125f)
    for (iter <- 1 until 5) {
      TraderService.addTrade(trader, newTrade)
    }
    assert(trader.account.points === 4)
  }

  test("clientHasBronzeMembershipAfterFourTrades") {
    val traderAccount = new Account()
    val trader = new Trader("John", "Doe", traderAccount)
    for (iter <- 1 until 5) {
      TraderService.addTrade(trader, new EquityTrade("uid32", "AAPL", 3, 3.2f, 0.125f))
    }
    assert(trader.account.points === 4)
    assert(trader.account.membershipType === MembershipType.BRONZE)
  }

  test("clientCantTradeAfterDailyTradeExhausted") {
    val traderAccount = new Account()
    val trader = new Trader("John", "Doe", traderAccount)
    for (iter <- 1 until 9) {
      TraderService.addTrade(trader, new EquityTrade("uid32", "AAPL", 3, 3.2f, 0.125f))
    }
    assert(trader.account.points === 5)
    assert(trader.account.membershipType === MembershipType.BRONZE)
  }


  test("bronzeClientCantTradeBefore10AM") {
    val traderAccount = new Account()
    val trader = new Trader("John", "Doe", traderAccount)

    val newTrade = new EquityTrade("uid32", "AAPL", 3, 3.2f, 0.125f)
    val newTrade1 = new EquityTrade("uid32", "AAPL", 3, 3.2f, 0.125f)

    TraderService.addTrade(trader, newTrade)

    assert(TraderService.addTrade(trader, newTrade1) == "You are not eligible to trade.")
  }


  test("clientHasSilverMembershipAfterTenTradesOverDifferentDays") {
    val traderAccount = new Account()
    val trader = new Trader("John", "Doe", traderAccount)
    val newTrade = new EquityTrade("uid32", "AAPL", 3, 3.2f, 0.125f)
    for (iter <- 1 until 7) {
      TraderService.addTrade(trader, newTrade)
    }
    assert(trader.account.points === 5)
    assert(trader.account.membershipType === MembershipType.BRONZE)

    val newTrade1 = new EquityTrade("uid32", "AAPL", 3, 3.2f, 0.125f)

    //change date on account
    trader.account.currentDate = LocalDate.now().minusDays(1)
    for (iter <- 1 until 7) {
      TraderService.addTrade(trader, newTrade1)
    }
    assert(trader.account.points === 11)
    assert(trader.account.membershipType === MembershipType.SILVER)
  }


  test("clientHasGoldMembershipAfterTwentyTradesOverDiffDays") {
    val traderAccount = new Account()
    val trader = new Trader("John", "Doe", traderAccount)
    val newTrade = new EquityTrade("uid32", "AAPL", 3, 3.2f, 0.125f)
    for (iter <- 1 until 6) {
      TraderService.addTrade(trader, newTrade)
    }
    assert(trader.account.points === 5)
    assert(trader.account.membershipType === MembershipType.BRONZE)


    val newTrade1 = new EquityTrade("uid32", "AAPL", 3, 3.2f, 0.125f)

    //change date on account
    trader.account.currentDate = LocalDate.now().minusDays(1)


    for (iter <- 1 until 11) {
      TraderService.addTrade(trader, newTrade1)
    }
    assert(trader.account.points === 15)


    val newTrade2 = new EquityTrade("uid32", "AAPL", 3, 3.2f, 0.125f)
    trader.account.currentDate = LocalDate.now.minusDays(1)

    for (iter <- 1 until 7) {
      TraderService.addTrade(trader, newTrade2)
    }
    assert(trader.account.points === 21)
    assert(trader.account.membershipType === MembershipType.GOLD)
  }
}
